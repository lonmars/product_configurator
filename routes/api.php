<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/getproduct', function () {
    return view('index');
});


Route::get('/get_sub_items', [
    'as'   => 'get_sub_itmes',
    'uses' => 'Controller@get_sub_product',
]);

Route::get('/get_item', [
    'as'   => 'get_product',
    'uses' => 'Controller@get_product',
]);

