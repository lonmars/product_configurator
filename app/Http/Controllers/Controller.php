<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Items;
use App\Items_category;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function get_sub_product(Request $request)
    {
    	
    	$items = Items_category::where('Items_categories.parent', $request->get('cat_id') )
    					->join('items', 'Items_categories.item_id', '=', 'items.id')
    					->select('Items_categories.id as cat_id', 'Items_categories.image as image','items.*')
    					->get();

    	return response()->json([
                    'status'  => 'Ok',
                    'data' => $items
                ]);

    	
    }

    public function get_product(Request $request)
    {
    	
    	$items = Items::where('id', $request->get('item_id') )
    					->first();

    	return response()->json([
                    'status'  => 'Ok',
                    'data' => $items
                ]);

    	
    }

}
