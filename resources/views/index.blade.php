<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="./favicon.ico">

    <title>Simple Product Configurator</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <!-- <link href="./css/form-validation.css" rel="stylesheet"> -->
  </head>

  <body class="bg-light">

    <div class="container">
      <div class="py-5 text-center">
        <!-- <img class="d-block mx-auto mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> -->
        <h2>Simple Product Configurator</h2>
      </div>

        <div class="row">

          <div class="col-md-4">
            
             ​<picture>
              <source id="img_src" srcset="./images/base.jpg" type="image/svg+xml">
              <img id="img_img" src="./images/base.jpg" class="img-fluid img-thumbnail" alt="./images/base.jpg">
            </picture>  

          </div>


         
          <div class="col-md-4 order-md-2 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
              <span class="text-muted">Your Option</span>
              
            </h4>
            <ul class="list-group mb-3">
              <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                  <h6 class="my-0">Arms</h6>
                  <p class="c_arms">
                  </p>
                </div>
              </li>
              
              <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                  <h6 class="my-0">Base</h6>
                  <p class="c_base">
                  
                  </p>
                </div>
              </li>

              <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                  <h6 class="my-0">Fabric</h6>
                  <p class="c_fabric">
             
                  </p>
                </div>
              </li>

             
            </ul>

          </div>




          <div class="col-md-4 order-md-5 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
              <span class="text-muted">Charges</span>
              
            </h4>
            <ul class="list-group mb-3">
              <li class="list-group-item d-flex justify-content-between">
                <span>Chair Body:</span>
                <div id="id_chairbody">$0</div>
              </li>

              <li class="list-group-item">
                <span>Add-Ons:</span>


                <ul class=" list-group-flush">
                  <li class="list-group-item d-flex justify-content-between">
                    Arm
                    <span>$<span id="id_arm">0<span></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between">
                    Base
                    <span>$<span id="id_base">0<span></span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between">
                    Fabric
                    <span>$<span id="id_fabric">0<span></span>
                  </li>
                 
                </ul>


              </li>

              <li class="list-group-item d-flex justify-content-between">
                <span>Sub Total:</span>
                <div id="id_subtotal">$0</div>
              </li>

              <li class="list-group-item d-flex justify-content-between">
                <span>Tax:</span>
                <div id="id_tax">$0</div>
              </li>

              <li class="list-group-item d-flex justify-content-between">
                <span>Total (USD)</span>
                <strong id="id_total">$0</strong>
              </li>
            </ul>

          </div>

        
        </div>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Marlon Pamisa</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
      </footer>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./bootstrap-4.0.0/assets/js/vendor/popper.min.js"></script>
    <script src="./bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
    <script src="./bootstrap-4.0.0/assets/js/vendor/holder.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script>
      ShowCategoryChild(1);
      var prodprice = 0;
      var arm = 0;
      var base =0;
      var fabric =0;
      function ShowCategoryChild(catid) {
        $.get( "http://localhost/Product_configurator/public/api/get_sub_items" ,
          {
            cat_id :catid 
          },
          function(data,status){
            //var data = JSON.parse(data); 
            console.log(data.data);
            var ht ="";
            for (var i = 0; i < data.data.length; i++) {
              
              ht+='<label>';
              ht+='  <input type="radio" name="arm" id="optionsRadios1" onclick="JavaScript:ShowCategoryChildArm('+  data.data[i].cat_id +', ' +data.data[i].amount +','+ "'" + data.data[i].image +"'"+');" value="'+  data.data[i].name +'">'
              ht+='    <small>'+ data.data[i].name +'</small>'
              ht+='  </label>'
            }
            $(".c_arms").html(ht);

          }
        );

      }

      function ShowCategoryChildArm(catid ,amount,image) {
        arm = amount;
        base =0;
        fabric=0;

        TotalSetter(image);

        $(".c_fabric").html("");
        $.get( "http://localhost/Product_configurator/public/api/get_sub_items" ,
          {
            cat_id : catid
          },
          function(data,status){
            //var data = JSON.parse(data); 
            console.log(data.data);
            var ht ="";
            for (var i = 0; i < data.data.length; i++) {
              
              ht+='<label>';
              ht+='  <input type="radio" name="base" id="baseid" onclick="JavaScript:ShowCategoryChildBase('+  data.data[i].cat_id +', ' +data.data[i].amount +','+ "'" + data.data[i].image +"'"+');" value="'+  data.data[i].name +'">'
              ht+='    <small>'+ data.data[i].name +'</small>'
              ht+='  </label>'
            }

            $(".c_base").html(ht);
          
          }
        );

      }

      function ShowCategoryChildBase(catid,amount,image) {
        base =amount;
        fabric=0;
        TotalSetter(image);
        $.get( "http://localhost/Product_configurator/public/api/get_sub_items" ,
          {
            cat_id : catid
          },
          function(data,status){
            //var data = JSON.parse(data); 
            console.log(data.data);
            var ht ="";
            for (var i = 0; i < data.data.length; i++) {
              
              ht+='<label>';
              ht+='  <input type="radio" name="fabric" id="baseid" onclick="JavaScript:ShowPrice('+  data.data[i].cat_id +', ' +data.data[i].amount +','+ "'" + data.data[i].image +"'"+');" value="'+  data.data[i].name +'">'
              ht+='    <small>'+ data.data[i].name +'</small>'
              ht+='  </label>'
            }

            $(".c_fabric").html(ht);
          }
        );

      }

      function ShowPrice(catid, amount,image) {
        fabric = amount;
        TotalSetter(image);
      }


      //Getting base Price

      $.get( "http://localhost/Product_configurator/public/api/get_item" ,
          {
            item_id : 1
          },
          function(data,status){
            prodprice = data.data.amount; 
            TotalSetter()          
          }
      );

      function TotalSetter(image) {

        $("#img_src").attr("srcset", image );
        $("#img_img").attr("img_img", image );
        $("#img_img").attr("alt", image );


        $("#id_chairbody").html('$'+ parseFloat(prodprice) );
        $("#id_arm").html(arm);
        $("#id_base").html(base);
        $("#id_fabric").html(fabric);
        $("#id_subtotal").html('$'+ ( parseFloat(prodprice) + parseFloat(arm) + parseFloat(base) + parseFloat(fabric) ) );
        $("#id_total").html('$'+ ( parseFloat(prodprice) + parseFloat(arm) + parseFloat(base) + parseFloat(fabric)  ) );

      }
    </script>
  </body>
</html>
